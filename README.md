Installation
=============


To build from the source run in the root of the project:

``mvn clean install``

This will produce fat-jar "calc.jar" to ``dist`` folder so you can run (from project root folder):

``java -jar ./dist/calc.jar``

or (from ``dist	>_`` folder):

``run.sh``

and for WIN

``run.bat``


*In case of the building problems you can download the built version*


Usage
=============

* Program will print usage at the beginning.
* Supported currencies are: EUR, USD, CHF, HKD, RMB


Implementation details
=============

* IOC - spring.
* Persistence: JPA ORM (hibernate provider) + in memory HSQL.
* Transactions (from keyboad, file, any source) go to JMS (spring + activeMQ) which are consumed after that.
* Main service layer is in the backend project (package process).
* For command line parsing I use *commons-cli*.
* For file processing *spring-batch* is used.
* For unit test demonstration - *JUnit, hamcrest, mockito*.

![Sequence diagram backend](http://content.screencast.com/users/Julls/folders/Jing/media/f53ca1a7-a9b9-4b26-a36a-1ff8bad4ef31/bpa.png)


Things that might be done better
=============

* Logging, javadoc, exception handling, maybe synchronization (some semaphores), performance of file processing can be improved by map-reduce (i.e. Apache hadoop).
* Might be packaged/moduled better but I didn't want to get things over-engineered/complicated for this demo.
* Supported curencies can be provided more intelligently.
* ...