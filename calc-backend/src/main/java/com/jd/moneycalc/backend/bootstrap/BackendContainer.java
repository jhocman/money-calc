package com.jd.moneycalc.backend.bootstrap;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jd.moneycalc.backend.model.Transaction;
import com.jd.moneycalc.backend.storage.DataStorage;

public class BackendContainer {
	
	private static ClassPathXmlApplicationContext container;

	static {
		bootContext();
	}

	private static void bootContext() {

		System.out.println("Starting backend container ....");
		
		container = new ClassPathXmlApplicationContext("spring-ctx.xml");

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				container.close();
			}
		});
	}
	
	public static void main(String[] args) throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		
		DataStorage bean = container.getBean(DataStorage.class);
		bean.saveTransaction(new Transaction());

	}

	public static ClassPathXmlApplicationContext getContainer() {
		return container;
	}

}
