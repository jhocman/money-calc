package com.jd.moneycalc.backend.common;

public interface Constants {

	String MESSAGE_QUEUE_NAME = "messageQueue";
	String TX_MANAGER = "jdbcTransactionManager";
	String FILE_URL = "file_url";

}
