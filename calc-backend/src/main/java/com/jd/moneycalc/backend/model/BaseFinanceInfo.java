package com.jd.moneycalc.backend.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseFinanceInfo {

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private BigDecimal amount;

	@Enumerated(EnumType.STRING)
	private CurrencyEnum currency;

	public BaseFinanceInfo() {

	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public CurrencyEnum getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyEnum currency) {
		this.currency = currency;
	}

}
