package com.jd.moneycalc.backend.model;

public enum CurrencyEnum {
	USD,
	EUR,
	CHF,
	HKD,
	RMB
}
