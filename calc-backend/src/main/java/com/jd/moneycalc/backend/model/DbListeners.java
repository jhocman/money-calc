package com.jd.moneycalc.backend.model;

import java.util.Date;

import javax.persistence.PrePersist;

public class DbListeners {

	@PrePersist
	public void prePersist(Transaction tx) {
		tx.setCreatedOn(new Date());
	}
}
