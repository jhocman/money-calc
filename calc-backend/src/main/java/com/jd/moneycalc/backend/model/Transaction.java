package com.jd.moneycalc.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

@Entity
@EntityListeners(DbListeners.class)
public class Transaction extends BaseFinanceInfo {

	@Column
	private Date createdOn;

	public Transaction() {
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
