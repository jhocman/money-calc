package com.jd.moneycalc.backend.process;

import java.math.BigDecimal;

import com.jd.moneycalc.backend.model.CurrencyEnum;

public interface CurrencyExchangeProvider {

	BigDecimal usdRateFor(CurrencyEnum currency);
}
