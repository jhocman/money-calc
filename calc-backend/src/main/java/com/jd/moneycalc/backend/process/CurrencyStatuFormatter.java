package com.jd.moneycalc.backend.process;

import java.math.BigDecimal;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.jd.moneycalc.backend.model.CurrencyEnum;
import com.jd.moneycalc.backend.model.CurrencyStatus;

@Component
public class CurrencyStatuFormatter {

	@Inject 
	CurrencyExchangeProvider exchangeProvider;

	public String format(CurrencyStatus currencyStatus) {

		StringBuilder buff = new StringBuilder(currencyStatus.getCurrency().name());
		buff.append(": " + printMoney(currencyStatus.getAmount(), currencyStatus.getCurrency()));
		BigDecimal rate = exchangeProvider.usdRateFor(currencyStatus.getCurrency());
		if (rate != null)
		{
			BigDecimal converted = currencyStatus.getAmount().divide(rate, 2, BigDecimal.ROUND_HALF_UP);
			buff.append(" (" + printMoney(converted, CurrencyEnum.USD ) + " "+ CurrencyEnum.USD + ")");
		}
		return buff.toString();
	}

	private String printMoney(BigDecimal amount, CurrencyEnum currency) {
		return amount + ""; // more advanced not now
	}

}
