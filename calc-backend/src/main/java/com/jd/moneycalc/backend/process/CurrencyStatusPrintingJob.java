package com.jd.moneycalc.backend.process;

import java.util.List;

import javax.inject.Inject;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jd.moneycalc.backend.model.CurrencyStatus;
import com.jd.moneycalc.backend.storage.DataStorage;

@Component
@EnableScheduling
public class CurrencyStatusPrintingJob {

	@Inject
	private DataStorage storage;

	@Inject
	private CurrencyStatuFormatter formatter;
	
	@Scheduled(/* delay init run after 10 secs to avoid mess the things on startup*/ initialDelay = 10000,
			fixedDelay = 30000)
	public void printReport() {
		
		System.err.println("\n\n===================================================================================================");
		System.err.println("==================================   CURRENCY STATUS REPORT  ======================================");
		List<CurrencyStatus> statuses = storage.getAllCurrencyStatuses();
		if (statuses.isEmpty())
		{
			System.err.println("===   CURRENTLY EMPTY  ===");
		}
		for (CurrencyStatus currencyStatus : statuses) {
			System.err.println(formatter.format(currencyStatus));
		}
		System.err.println("==================================================================================================\n\n");
	}

}
