package com.jd.moneycalc.backend.process;

import javax.inject.Inject;

import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.moneycalc.backend.common.Constants;
import com.jd.moneycalc.backend.model.CurrencyStatus;
import com.jd.moneycalc.backend.model.Transaction;
import com.jd.moneycalc.backend.storage.DataStorage;

@Service
@EnableJms
public class JmsCalcListener {

	@Inject
	private TransactionAssembler assembler;

	@Inject
	private DataStorage storage;

	@JmsListener(destination = Constants.MESSAGE_QUEUE_NAME)
	@Transactional(transactionManager = Constants.TX_MANAGER)
	public void processSumming(String data) {
	
		System.out.println("Got JMS message ('" + data + "').");
	
		Transaction tx = assembler.assemble(data);
		
		// save
		saveTransaction(tx);

		// sum
		sumToCurrency(tx);
	}

	private void sumToCurrency(Transaction tx) {
		CurrencyStatus currencyStatus = storage.getCurrencyStatusForCurrency(tx.getCurrency());
		if (currencyStatus != null)
		{
			currencyStatus.setAmount(currencyStatus.getAmount().add(tx.getAmount()));
			System.out.println(">>> Updating currency " + tx.getCurrency() + " sum to " + currencyStatus.getAmount() + ".");
		}
		else
		{
			currencyStatus = new CurrencyStatus();
			currencyStatus.setAmount(tx.getAmount());
			currencyStatus.setCurrency(tx.getCurrency());
			System.out.println(">>> Creating new currency " + tx.getCurrency() + " sum.");
		}
		
		storage.saveStatus(currencyStatus);
	}

	private void saveTransaction(Transaction tx) {
		storage.saveTransaction(tx);
	}

}