package com.jd.moneycalc.backend.process;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.jms.config.AbstractJmsListenerContainerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

@Component
public class JmsErrorHandler implements ErrorHandler {

	@Inject
	private AbstractJmsListenerContainerFactory<?> jmsFactory;

    @Override
    public void handleError(Throwable t) {

    	Throwable mainCause = t.getCause();
		System.err.println(mainCause.getMessage());

		if (mainCause.getCause() != null)
		{
			System.err.println("Details : " + mainCause.getCause().getMessage());
		}
    	// probably more logs here
    }

    @PostConstruct
    public void afterInit()
    {
    	jmsFactory.setErrorHandler(this);
    }
}