package com.jd.moneycalc.backend.process;

import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;
import org.springframework.jms.core.JmsTemplate;

import com.jd.moneycalc.backend.common.Constants;

public class JmsWritingMessageItemWriter implements ItemWriter<String> {

	@Inject
	private JmsTemplate jms;

	public void write(List<? extends String> transactions) throws Exception {

		for (String tx : transactions) {
			jms.convertAndSend(Constants.MESSAGE_QUEUE_NAME, tx);
		}
	}

}
