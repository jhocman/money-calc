package com.jd.moneycalc.backend.process;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jd.moneycalc.backend.common.Constants;

@Component
public class ProcessFileJob {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private Job processTxFileJob;

	public void processFile(String filePath) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addString(Constants.FILE_URL, filePath);

		System.out.println("Got file to process: " + filePath);

		try {
			jobLauncher.run(processTxFileJob, builder.toJobParameters());
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException e) {
			System.err.println("Error launching file processing.");
			e.printStackTrace();
		}
	}
}
