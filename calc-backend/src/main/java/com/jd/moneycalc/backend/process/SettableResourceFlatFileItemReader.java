package com.jd.moneycalc.backend.process;


import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.core.io.FileSystemResource;

import com.jd.moneycalc.backend.common.Constants;

public class SettableResourceFlatFileItemReader extends FlatFileItemReader<String> {

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) throws Exception
	{
		String url = (String) stepExecution.getJobParameters().getString(Constants.FILE_URL);
		setResource(new FileSystemResource(url));
	}
}
