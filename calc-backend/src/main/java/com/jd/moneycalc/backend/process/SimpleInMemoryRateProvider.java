package com.jd.moneycalc.backend.process;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import com.jd.moneycalc.backend.model.CurrencyEnum;

@Component
public class SimpleInMemoryRateProvider implements CurrencyExchangeProvider {

	@SuppressWarnings("incomplete-switch")
	public BigDecimal usdRateFor(CurrencyEnum currency) {
		
		switch(currency) {
			case EUR: return new BigDecimal("0.90");
			case CHF: return new BigDecimal("0.98");
			case HKD: return new BigDecimal("7.76");
			case RMB: return new BigDecimal("6.68");
		}
		return null;
	}

}
