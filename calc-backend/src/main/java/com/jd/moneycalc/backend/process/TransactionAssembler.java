package com.jd.moneycalc.backend.process;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.jd.moneycalc.backend.model.CurrencyEnum;
import com.jd.moneycalc.backend.model.Transaction;

@Component
public class TransactionAssembler {

	static final Pattern WHITESPACE_REX = Pattern.compile("\\s");

	Transaction assemble(String txAsString) throws AdapterException {

		Transaction tx = new Transaction();

		BigDecimal amount;
		CurrencyEnum currency;
		try {
			String[] tokens = WHITESPACE_REX.split(txAsString);
			amount = new BigDecimal(tokens[0]);
			currency = CurrencyEnum.valueOf(tokens[1]);

			// fill tx
			tx.setAmount(amount);
			tx.setCurrency(currency);

		} catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
			throw new AdapterException("Illegal transaction string.", new IllegalArgumentException(ex));
		} catch (IllegalArgumentException iae) {
			throw new AdapterException("Unsupported currency.", iae);
		} catch (Exception e) {
			throw new AdapterException("Unexpected error.", e);
		}

		return tx;
	}

	static class AdapterException extends RuntimeException {
		private static final long serialVersionUID = -2898174278993187162L;

		public AdapterException() {
			super();
		}

		public AdapterException(String message, Throwable cause, boolean enableSuppression,
				boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}

		public AdapterException(String message, Throwable cause) {
			super(message, cause);
		}

		public AdapterException(String message) {
			super(message);
		}

		public AdapterException(Throwable cause) {
			super(cause);
		}

	}
}
