package com.jd.moneycalc.backend.storage;

import java.util.List;

import com.jd.moneycalc.backend.model.CurrencyEnum;
import com.jd.moneycalc.backend.model.CurrencyStatus;
import com.jd.moneycalc.backend.model.Transaction;

public interface DataStorage {

	void saveTransaction(Transaction tx);

	void saveStatus(CurrencyStatus status);
	
	CurrencyStatus getCurrencyStatusForCurrency(CurrencyEnum currency);

	List<CurrencyStatus> getAllCurrencyStatuses();

	
}
