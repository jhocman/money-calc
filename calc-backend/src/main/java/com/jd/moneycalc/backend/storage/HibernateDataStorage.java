package com.jd.moneycalc.backend.storage;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.jd.moneycalc.backend.common.Constants;
import com.jd.moneycalc.backend.model.CurrencyEnum;
import com.jd.moneycalc.backend.model.CurrencyStatus;
import com.jd.moneycalc.backend.model.Transaction;

@Component
@EnableTransactionManagement
@Transactional(transactionManager = Constants.TX_MANAGER)
public class HibernateDataStorage implements DataStorage {

	@PersistenceContext
	private EntityManager em;

	public void saveTransaction(Transaction tx) {
		em.persist(tx);
	}

	public void saveStatus(CurrencyStatus status) {
		em.persist(status);
	}

	public CurrencyStatus getCurrencyStatusForCurrency(CurrencyEnum currency) {

		TypedQuery<CurrencyStatus> query = em.createQuery("from CurrencyStatus p where currency = :c",
				CurrencyStatus.class);
		query.setParameter("c", currency);

		List<CurrencyStatus> resultList = query.getResultList();
		return resultList.size() == 1 ? resultList.get(0) : null;
	}

	public List<CurrencyStatus> getAllCurrencyStatuses() {

		TypedQuery<CurrencyStatus> query = em.createQuery("from CurrencyStatus",
				CurrencyStatus.class);

		return query.getResultList();
	}

}
