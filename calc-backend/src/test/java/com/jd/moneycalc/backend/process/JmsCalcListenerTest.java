package com.jd.moneycalc.backend.process;

import java.math.BigDecimal;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jd.moneycalc.backend.model.CurrencyEnum;
import com.jd.moneycalc.backend.model.CurrencyStatus;
import com.jd.moneycalc.backend.model.Transaction;
import com.jd.moneycalc.backend.storage.DataStorage;

@RunWith(MockitoJUnitRunner.class)
public class JmsCalcListenerTest {

	@Mock
	private TransactionAssembler assembler;

	@Mock
	private DataStorage storage;

	@Captor
	ArgumentCaptor<CurrencyStatus> savedStatus;
	
	@InjectMocks
	private JmsCalcListener listener;

	@Test
	public void processSumming_correctTxString_containsRecordForCurrency_ShouldPassOK()
	{
		String txSampleAsString = "150.23 EUR";

		Mockito.when(storage.getCurrencyStatusForCurrency(CurrencyEnum.EUR)).thenReturn(sampleCurrencyStatus());
		Mockito.when(assembler.assemble(txSampleAsString)).thenReturn(sampleTransaction("150.23", CurrencyEnum.EUR));
		
		listener.processSumming(txSampleAsString);
	
		Mockito.verify(storage).saveStatus(savedStatus.capture());
		Mockito.verify(assembler).assemble(txSampleAsString);
		
		Assert.assertThat(savedStatus.getValue().getAmount(), Matchers.is(new BigDecimal("250.23")));
		Assert.assertThat(savedStatus.getValue().getCurrency(), Matchers.is(CurrencyEnum.EUR));
	}

	private Transaction sampleTransaction(String amount, CurrencyEnum currency) {
		Transaction tx = new Transaction();
		tx.setAmount(new BigDecimal(amount));
		tx.setCurrency(currency);
		return tx;
	}

	private CurrencyStatus sampleCurrencyStatus() {
		CurrencyStatus status = new CurrencyStatus();
		status.setCurrency(CurrencyEnum.EUR);
		status.setAmount(new BigDecimal("100"));
		return status;
	}
}