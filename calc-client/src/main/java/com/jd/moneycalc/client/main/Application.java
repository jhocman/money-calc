package com.jd.moneycalc.client.main;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.jd.moneycalc.backend.bootstrap.BackendContainer;
import com.jd.moneycalc.backend.process.ProcessFileJob;

public class Application {

	private static final String TX_OPT = "tx";
	private static final String VERBOSE_OPT = "v";
	private static ProcessFileJob fileProcessor;
	private static CommandLine line;

	public static void main(String[] args) {

		line = getParsedCommandLine(args);

		// check verbose
		if (line.hasOption(VERBOSE_OPT)) {
			System.getProperties().put("verbose", "true");
		}

		init();

		// scan input
		new LineScanner().start();

		// check file with TXs passed
		checkFilePassed(line);
	}

	private static CommandLine getParsedCommandLine(String[] args) {
		CommandLineParser parser = new DefaultParser();
		try {

			return parser.parse(createOptions(), args);

		} catch (ParseException e) {
			System.out.println("Invalid args. System terminates.");
			printUsage();
			System.exit(1);
			return null;
		}
	}

	private static void checkFilePassed(CommandLine line) {
		if (line.hasOption(TX_OPT)) {
			String filePath = line.getOptionValue(TX_OPT);
			fileProcessor.processFile(filePath);
		}
	}

	private static void printUsage() {

		Options opts = createOptions();

		final HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("java -jar calc.jar <options>:", opts);

	}

	private static Options createOptions() {
		Option file = createFileOption();
		Option verbose = createVerboseOption();

		Options opts = new Options();
		opts.addOption(file);
		opts.addOption(verbose);
		return opts;
	}

	private static Option createFileOption() {
		return Option.builder(TX_OPT).longOpt("transactionFile").hasArg()
				.desc("Optional: use file with transactions to be processed (absolute path).").build();
	}

	private static Option createVerboseOption() {
		return Option.builder(VERBOSE_OPT).longOpt("verbose").desc("Optional: use this to get SQL commands shown.")
				.build();
	}

	private static void init() {
		// usage info
		System.out.println(
				"========================================== USAGE =============================================");
		printUsage();
		System.out.println(
				"========================================== ==== ==============================================");
		fileProcessor = BackendContainer.getContainer().getBean(ProcessFileJob.class);
	}

}
