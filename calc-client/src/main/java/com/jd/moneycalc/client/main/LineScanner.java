package com.jd.moneycalc.client.main;

import java.util.Scanner;

import org.springframework.jms.core.JmsTemplate;

import com.jd.moneycalc.backend.bootstrap.BackendContainer;
import com.jd.moneycalc.backend.common.Constants;

public class LineScanner extends Thread {

	private JmsTemplate jms;

	public LineScanner() {
		jms = BackendContainer.getContainer().getBean(JmsTemplate.class);
	}
	
	@Override
	public void run() {
		
		System.out.println("\n\n\n\nScanning input for transactions... ");
		System.out.println("Format is: <AMOUNT> <CURRENCY>");
		System.out.println("Type 'end' to end scanning.\n\n\n\n");

		Scanner scanner = new Scanner(System.in);
		String scan = "";
		
		while (!(scan = scanner.nextLine()).equalsIgnoreCase("end"))
		{
			jms.convertAndSend(Constants.MESSAGE_QUEUE_NAME, scan);
		}

		System.out.println("bye...");
		scanner.close();
	}
}
